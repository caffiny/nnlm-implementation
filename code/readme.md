# NNLM 复现



## Depend on

|    Library     | Version |
| :------------: | :-----: |
| tensorflow-gpu | 1.14.0  |
|     numpy      | 1.20.3  |
|      math      |    -    |
|     pickle     |    -    |



## 主要参数说明

* dataProcess.py / textLoader()
  - dataDir - 数据集路径
  - batchSize - batch大小
  - seqLength - 预测句子的fixed窗口+1
  - minFrq - 出现频率小于这个值的词将被过滤

* model.py / main()
  * hiddenSize - 隐藏层大小，论文中给出的是64
  * wordDim - 词特征大小，论文给出的是50
  * threshold - 梯度阈值



## 函数说明

* model.py / test()
  * 加载NNLM训练完后保存的模型，然后随机从dataLoader中挑选sample进行预测下一个词
* model.py / try2MS()
  * 加载NNLM训练完后保存的模型，然后随机从dataLoader中挑选sample进行往下造句（相当于往后连续预测num2MS个词语）