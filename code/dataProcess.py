import os
import pickle
import collections
import numpy as np


class textLoader():
    def __init__(self, dataDir, batchSize, seqLength, miniFrq=3):
        self.dataDir = dataDir
        self.batchSize = batchSize
        self.seqLength = seqLength-1
        self.miniFrq = miniFrq
        self.vocabSize = 0
        self.rawData = None
        self.xData = None
        self.yData = None
        self.pointer = 0
        self.numBatch = 0

        self.preProcess(inputFile=os.path.join(dataDir, 'input.en.txt'),
                        vocabFile=os.path.join(dataDir, 'vocab.en.pkl'))
        self.createBatches()

    def buildVocab(self, sentenceList):
        Counter = collections.Counter()
        for sentence in sentenceList:
            Counter.update(sentence)
        idx2wordList = ['<START>', 'UNK', '<END>'] + \
                       [pair[0] for pair in Counter.most_common() if pair[1] >= self.miniFrq]
        word2idxDict = {word: idx for idx, word in enumerate(idx2wordList)}
        return word2idxDict, idx2wordList

    def preProcess(self, inputFile, vocabFile):
        with open(inputFile, 'r') as rd:
            lines = rd.readlines()
            lines = [line.strip().split() for line in lines]
        rd.close()
        w2i, i2w = self.buildVocab(lines)
        self.vocabSize = len(i2w)
        with open(vocabFile, 'wb') as wrt:
            pickle.dump(i2w, wrt)
        wrt.close()
        self.rawData = [
            [0] * self.seqLength +
            [w2i.get(w, 1) for w in line] +
            [2] * self.seqLength
            for line in lines
        ]

    def createBatches(self):
        xData, yData = [], []
        for oneRow in self.rawData:
            for idx in range(self.seqLength, len(oneRow)):
                xData.append(oneRow[idx-self.seqLength:idx])
                yData.append([oneRow[idx]])
        self.numBatch = int(len(xData) / self.batchSize)
        xData = np.array(xData[:self.numBatch * self.batchSize])
        yData = np.array(yData[:self.numBatch * self.batchSize])
        self.xData = np.split(xData, self.numBatch, 0)
        self.yData = np.split(yData, self.numBatch, 0)

    def nextBatch(self):
        x, y = self.xData[self.pointer], self.yData[self.pointer]
        self.pointer += 1
        return x, y

    def resetPointer(self, num):
        self.pointer = num
