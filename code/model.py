import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import math
import pickle
import argparse
import numpy as np
import tensorflow as tf
from dataProcess import textLoader


class NNLM:
    def __init__(self, batchSize, nGram, vocabSize, wordDim, hiddenSize, numEpoch, lr, threshold):
        """Hyper parameters"""
        self.numEpoch = numEpoch
        self.lr = lr
        self.threshold = threshold
        """for feedDict"""
        self.inputWords = tf.placeholder(dtype=tf.int32, shape=[batchSize, nGram-1])
        self.outputWord = tf.placeholder(dtype=tf.int32, shape=[batchSize, 1])
        """Updated parameters"""
        self.para_C = tf.Variable(initial_value=tf.nn.l2_normalize(tf.random_uniform(shape=(vocabSize, wordDim),
                                                                                     minval=-1.0, maxval=1.0), axis=1),
                                  name='Word_Embedding')
        self.para_H = tf.Variable(initial_value=tf.truncated_normal(shape=[hiddenSize, (nGram-1)*wordDim],
                                                                    stddev=1.0 / math.sqrt(hiddenSize)),
                                  name='Hidden_Weights')
        self.para_d = tf.Variable(initial_value=tf.random_normal(shape=(hiddenSize, )),
                                  name='Hidden_Bias')
        self.para_U = tf.Variable(initial_value=tf.truncated_normal(shape=[vocabSize, hiddenSize],
                                                                    stddev=1.0 / math.sqrt(hiddenSize)),
                                  name='tanh_Weights')
        self.para_b = tf.Variable(initial_value=tf.random_normal(shape=(vocabSize, )),
                                  name='Output_Bias')
        self.para_W = tf.Variable(initial_value=tf.truncated_normal(shape=(vocabSize, (nGram-1)*wordDim),
                                                                    stddev=1.0 / math.sqrt((nGram-1)*wordDim)),
                                  name='P2O_Weights')

        """Calculate y = b + Wx + Utanh(d + Hx)"""
        # 1. word_idx -> C[i] -> x
        with tf.name_scope('Projection_Layer'):
            x = tf.nn.embedding_lookup(self.para_C, self.inputWords)      # https://www.jianshu.com/p/6e61528acad9
            x = tf.reshape(x, (batchSize, (nGram-1)*wordDim))
        # 2. x -> tanh()
        with tf.name_scope('Hidden_Layer'):
            Hx = tf.matmul(x, tf.transpose(self.para_H))      # Hx
            Hxd = tf.add(self.para_d, Hx)       # Hx + d
            tanh = tf.nn.tanh(Hxd)       # tanh(d+Hx)
        # 3. Utanh() + Wx + b
        with tf.name_scope('Output_Layer'):
            Ut = tf.matmul(tanh, tf.transpose(self.para_U))      # Utanh()
            Wx = tf.matmul(x, tf.transpose(self.para_W))     # Wx
            self.out1 = tf.clip_by_value(tf.add(self.para_b, tf.add(Wx, Ut)), 0.0, 10.0)
            self.out2 = tf.nn.softmax(self.out1)
        # 4. Loss
        with tf.name_scope('Loss'):
            # 1
            ohLabel = tf.one_hot(tf.squeeze(self.outputWord), vocabSize)
            self.loss1 = -1 * tf.reduce_mean(tf.reduce_sum(tf.log(self.out2)*ohLabel, 1))
            # 2
            # self.loss2 = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.out1,
            #                                                                        labels=self.outputWord))
        """optimize"""
        self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.lr)
        # clip gradient
        gvs = self.optimizer.compute_gradients(self.loss1)
        capped_gvs = [(tf.clip_by_value(grad, -self.threshold, self.threshold), var) for grad, var in gvs]
        self.optimizer = self.optimizer.apply_gradients(capped_gvs)

        """predict"""
        self.predictBatch = tf.argmax(self.out2, 1)

    def train(self, dataLoader):
        with tf.Session() as sess:
            initializer = tf.global_variables_initializer()
            initializer.run()
            for epoch in range(self.numEpoch):
                dataLoader.resetPointer(num=0)
                print('='*25 + ' {}th Epoch '.format(epoch) + '='*25)
                for step in range(dataLoader.numBatch):
                    xBatch, yBatch = dataLoader.nextBatch()
                    feedDict = {self.inputWords: xBatch, self.outputWord: yBatch}
                    fetches = [self.loss1, self.optimizer]
                    loss, _ = sess.run(fetches, feedDict)
                    if step % 100 == 0:
                        print('Step {}/{}, Loss: {:.5f}'.format(step, dataLoader.numBatch, loss))

            saver = tf.train.Saver()
            saver.save(sess=sess, save_path='./model/epoch{}-lr{}/model'.format(self.numEpoch, self.lr))
            wordEmbedding = sess.run(self.para_C)
            np.save('./model/epoch{}-lr{}/wordEmb'.format(self.numEpoch, self.lr), wordEmbedding)
        print('\n'+'*'*50 + '\n', 'Training Done.')

    def test(self, dataLoader, numEpoch, lr):
        dataLoader.resetPointer(num=np.random.randint(dataLoader.numBatch))
        xBatch, yBatch = dataLoader.nextBatch()
        predBatch = self.predict(xBatch, numEpoch, lr)[0]
        with open('./data/vocab.en.pkl', 'rb') as rd:
            i2w = pickle.load(rd)
        rd.close()
        print('*'*25, '-- START --', '*'*25)
        for i in np.random.randint(0, len(xBatch), size=(10, )):
            oneRow = xBatch[i]
            for w in oneRow:
                print(i2w[w], end=' ')
            print('-> {}'.format(i2w[yBatch[i][0]]))
            print('Predict is:\t[\'{}\']\n'.format(i2w[predBatch[i]]))

    def predict(self, inputBatch, numEpoch, lr):
        saver = tf.train.Saver()
        with tf.Session() as sess:
            saver.restore(sess, './model/epoch{}-lr{}/model'.format(numEpoch, lr))
            predictBatch = sess.run([self.predictBatch], feed_dict={self.inputWords: inputBatch})
            return predictBatch

    def try2MS(self, dataLoader, numEpoch, lr, num2MS):
        with open('./data/vocab.en.pkl', 'rb') as rd:
            i2w = pickle.load(rd)
        rd.close()
        dataLoader.resetPointer(num=np.random.randint(dataLoader.numBatch))
        xBatch, _ = dataLoader.nextBatch()
        xOne = xBatch[np.random.randint(len(xBatch))]
        contain = []
        for w in xOne:
            contain.append(i2w[w])
        contain.append('->')
        for _ in range(num2MS):
            pred = self.predict(xBatch, numEpoch, lr)[0]
            contain.append(i2w[pred[0]])
            xOne = np.append(xBatch[0][1:], pred[0])
            xBatch[0] = xOne
        for w in contain:
            print(w, end=' ')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataDir', type=str, default='data/', help='your data directory')
    parser.add_argument('--batchSize', type=int, default=128, help='size of one batch')
    parser.add_argument('--nGram', type=int, default=5, help='size of fixed windows')
    parser.add_argument('--hiddenSize', type=int, default=64, help='number of neurons in hidden layer')
    parser.add_argument('--wordDim', type=int, default=50, help='number of word features')
    parser.add_argument('--numEpoch', type=int, default=10, help='number of epochs')
    parser.add_argument('--threshold', type=float, default=10., help='clip gradients at this value')
    parser.add_argument('--lr', type=float, default=0.1, help='learning rate')
    args = parser.parse_args()

    dataLoader = textLoader(args.dataDir, args.batchSize, args.nGram)
    vocabSize = dataLoader.vocabSize

    model = NNLM(batchSize=args.batchSize, nGram=args.nGram, vocabSize=vocabSize, wordDim=args.wordDim,
                 hiddenSize=args.hiddenSize, lr=args.lr, numEpoch=args.numEpoch, threshold=args.threshold)
    # model.train(dataLoader)
    # model.test(dataLoader, args.numEpoch, args.lr)
    print('*' * 25, '-- START --', '*' * 25)
    for _ in range(5):
        model.try2MS(dataLoader, args.numEpoch, args.lr, 5)
        print()


main()
